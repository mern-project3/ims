const express = require("express");
const cors = require("cors");
const dotenv = require("dotenv").config();
const mongoose = require("mongoose");
const colors = require("colors");
const { errorHandler } = require("./middlewares/errorMiddleware");
const port = process.env.PORT || 4000;
const userRoutes = require("./routes/user");
const staffRoutes = require("./routes/staff");
const assetRoutes = require("./routes/asset");
const ipRoutes = require("./routes/ip");
const printerRoutes = require("./routes/printer");

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
mongoose.connection.once("open", () =>
  console.log("Now connected to MongoDB Atlas")
);
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use("/users", userRoutes);
app.use("/staffs", staffRoutes);
app.use("/assets", assetRoutes);
app.use("/ips", ipRoutes);
app.use("/printers", printerRoutes);

// 404 NOT FOUND
app.all("*", (req, res) => {
  res.send({
    message: "Something wen't wrong. The page you are looking for is not found",
  });
});

app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server is now running on port ${port}`.cyan);
});

module.exports = app;
