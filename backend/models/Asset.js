const mongoose = require("mongoose");

const assetSchema = new mongoose.Schema(
  {
    assetNumber: {
      type: String,
      unique: true,
      required: [true, "Asset number is required"],
    },
    serialNumber: {
      type: String,
      default: null,
    },
    assignedTo: {
      type: String,
      default: null,
    },
    location: {
      type: String,
      default: null,
    },
    hostName: {
      type: String,
      default: "vacant",
    },
    typeOfDevice: {
      type: String,
      required: [true, "Type of device is required"],
    },
    operatingSystem: {
      type: String,
      default: null,
    },
    ram: {
      type: String,
      default: null,
    },
    ssd: {
      type: String,
      default: null,
    },
    hdd: {
      type: String,
      default: null,
    },
    bitlockerPin: {
      type: String,
      default: null,
    },
    bitlockerRecovery: {
      type: String,
      default: null,
    },
    installedApps: {
      type: Array,
      default: [],
    },
    status: {
      type: String,
      default: "working",
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Asset", assetSchema);
