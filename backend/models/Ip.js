const mongoose = require("mongoose");

const ipSchema = new mongoose.Schema(
  {
    ipAddress: {
      type: String,
      default: null,
    },
    description: {
      type: String,
      default: null,
    },
    assignedTo: {
      type: String,
      default: null,
    },
    typeOfDevice: {
      type: String,
      default: null,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("IP", ipSchema);
