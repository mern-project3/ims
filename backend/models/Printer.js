const mongoose = require("mongoose");

const printerSchema = new mongoose.Schema(
  {
    assetNumber: {
      type: String,
      required: [true, "Asset number is required"],
    },
    serialNumber: {
      type: String,
      default: null,
    },
    model: {
      type: String,
      default: null,
    },
    description: {
      type: String,
      default: null,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Printer", printerSchema);
