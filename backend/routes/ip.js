const express = require("express");
const router = express.Router();
const auth = require("../auth");
const {
  getIps,
  addIp,
  showIp,
  updateIp,
  deleteIp,
} = require("../controllers/ip");

router.get("/", auth.verify, getIps);
router.get("/:id", auth.verify, showIp);
router.post("/", auth.verify, addIp);
router.put("/:id/update", auth.verify, updateIp);
router.delete("/:id/delete", auth.verify, deleteIp);

module.exports = router;
