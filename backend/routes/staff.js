const express = require("express");
const router = express.Router();
const auth = require("../auth");
const {
  getStaffs,
  addStaff,
  showStaff,
  updateStaff,
  disableEnableStaff,
  deleteStaff,
} = require("../controllers/staff");

router.get("/", auth.verify, getStaffs);
router.get("/:id", auth.verify, showStaff);
router.post("/", auth.verify, addStaff);
router.put("/:id/update", auth.verify, updateStaff);
router.put("/:id/change-staff-status", auth.verify, disableEnableStaff);
router.delete("/:id/delete", auth.verify, deleteStaff);

module.exports = router;
