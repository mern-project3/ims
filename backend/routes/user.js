const express = require("express");
const router = express.Router();
const auth = require("../auth");
const {getUsers, registerUser, loginUser, showUser, updateUser, disableEnableUser} = require("../controllers/user");

router.get("/",auth.verify, getUsers);
router.get("/:id",auth.verify, showUser);
router.post("/register", registerUser);
router.post("/login", loginUser);
router.put("/:id/update", auth.verify, updateUser);
router.put("/:id/change-user-status", auth.verify, disableEnableUser);

module.exports = router;