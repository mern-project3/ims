const express = require("express");
const router = express.Router();
const auth = require("../auth");
const {
  getPrinters,
  addPrinter,
  showPrinter,
  updatePrinter,
  deletePrinter,
} = require("../controllers/printer");

router.get("/", auth.verify, getPrinters);
router.get("/:id", auth.verify, showPrinter);
router.post("/", auth.verify, addPrinter);
router.put("/:id/update", auth.verify, updatePrinter);
router.delete("/:id/delete", auth.verify, deletePrinter);

module.exports = router;
