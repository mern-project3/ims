const express = require("express");
const router = express.Router();
const auth = require("../auth");
const {
  getAssets,
  addAsset,
  showAsset,
  updateAssset,
  addInstalledApp,
  deleteInstalledApp,
} = require("../controllers/asset");

router.get("/", getAssets);
router.get("/:id", showAsset);
router.post("/", auth.verify, addAsset);
router.put("/:id/update", auth.verify, updateAssset);
router.put("/:id/add-apps", auth.verify, addInstalledApp);
router.put("/:id/delete-apps", auth.verify, deleteInstalledApp);

module.exports = router;
