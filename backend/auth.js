const jwt = require("jsonwebtoken");
// User defined string data that will be used to create our JSON web tokens
// Used in algo for encrypting our data which makes it difficukt to decode the information without the defined secret keyword
const secret = "imsAPI";

// [SECTION] JSON Web Tokens

// Token Creation
module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  // Generate a JSON web token using the jwt's sign method
  return jwt.sign(data, secret, {});
};

// Token Verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== "undefined") {
    // console.log(token);
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({ auth: "Authentication Failed" });
      } else {
        next();
      }
    });
  } else {
    return res.send({ auth: "Authentication Failed" });
  }
};

// Token Decryption
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }.payload);
      }
    });
  } else {
    return null;
  }
};
