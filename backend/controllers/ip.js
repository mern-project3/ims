const asyncHandler = require("express-async-handler");
const IP = require("../models/IP");
const colors = require("colors");

const getIps = asyncHandler(async (req, res) => {
  let ips = await IP.find({});
  res.status(200).json(ips);
});

const addIp = asyncHandler(async (req, res) => {
  let findIp = await IP.find({
    ipAddress: req.body.ipAddress,
  });

  if (findIp.length !== 0) {
    res.status(409);
    throw new Error("Duplicate record");
  } else {
    const ip = await IP.create({
      ipAddress: req.body.ipAddress,
      description: req.body.description,
      typeOfDevice: req.body.typeOfDevice,
    });
    res.status(201).json(ip);
  }
});

const showIp = asyncHandler(async (req, res) => {
  let ip = await IP.findById(req.params.id);
  res.status(200).json(ip);
});

const updateIp = asyncHandler(async (req, res) => {
  let ip = await IP.findById(req.params.id);
  if (!ip) {
    res.status(400);
    throw new Error("Ip not found");
  }

  const updatedIp = await IP.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  res.status(200).json(updatedIp);
});

const deleteIp = asyncHandler(async (req, res) => {
  let ip = await IP.findById(req.params.id);
  if (!ip) {
    res.status(400);
    throw new Error("Ip not found");
  }

  if (ip.assignedTo !== null) {
    res.status(400);
    throw new Error("Ip is currently in use");
  } else {
    const deletedIp = await IP.findByIdAndDelete(req.params.id);
    res.status(200).json(deletedIp);
  }
});

module.exports = {
  getIps,
  addIp,
  showIp,
  updateIp,
  deleteIp,
};
