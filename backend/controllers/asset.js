const asyncHandler = require("express-async-handler");
const Asset = require("../models/Asset");
const colors = require("colors");

const getAssets = asyncHandler(async (req, res) => {
  let assets = await Asset.find({});
  res.status(200).json(assets);
});

const addAsset = asyncHandler(async (req, res) => {
  let findAsset = await Asset.find({
    assetNumber: req.body.assetNumber,
  });

  if (findAsset.length !== 0) {
    res.status(409);
    throw new Error("Duplicate record");
  } else {
    const asset = await Asset.create({
      assetNumber: req.body.assetNumber,
      typeOfDevice: req.body.typeOfDevice,
    });
    res.status(201).json(asset);
  }
});

const showAsset = asyncHandler(async (req, res) => {
  let asset = await Asset.findById(req.params.id);
  res.status(200).json(asset);
});

const updateAssset = asyncHandler(async (req, res) => {
  let asset = await Asset.findById(req.params.id);
  if (!asset) {
    res.status(400);
    throw new Error("Asset not found");
  }

  const updatedAsset = await Asset.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  res.status(200).json(updatedAsset);
});

const addInstalledApp = asyncHandler(async (req, res) => {
  let asset = await Asset.findById(req.params.id);
  if (!asset) {
    res.status(400);
    throw new Error("Asset not found");
  }

  let newApps = req.body.installedApps;

  newApps.forEach((app) => {
    if (!asset.installedApps.includes(app)) {
      asset.installedApps.push(app);
      console.log(
        `[${app} added successfully on asset ${
          asset.assetNumber
        } - ${new Date()}]`.green
      );
    }
  });
  const updatedAsset = await Asset.findByIdAndUpdate(
    req.params.id,
    { installedApps: asset.installedApps },
    {
      new: true,
    }
  );
  res.status(200).json(updatedAsset);
});

const deleteInstalledApp = asyncHandler(async (req, res) => {
  let asset = await Asset.findById(req.params.id);
  if (!asset) {
    res.status(400);
    throw new Error("Asset not found");
  }

  let appToDelete = req.body.deleteApps;
  appToDelete.forEach((app) => {
    if (asset.installedApps.includes(app)) {
      const appIndex = asset.installedApps.indexOf(app);
      if (appIndex > -1) {
        asset.installedApps.splice(appIndex, 1);
        console.log(
          `[${app} removed successfully on asset ${
            asset.assetNumber
          } - ${new Date()}]`.red
        );
      }
    }
  });

  const updatedAsset = await Asset.findByIdAndUpdate(
    req.params.id,
    { installedApps: asset.installedApps },
    {
      new: true,
    }
  );
  res.status(200).json(updatedAsset);
});

module.exports = {
  getAssets,
  addAsset,
  showAsset,
  updateAssset,
  addInstalledApp,
  deleteInstalledApp,
};
