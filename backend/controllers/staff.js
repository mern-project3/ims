const asyncHandler = require("express-async-handler");
const Staff = require("../models/Staff");
const colors = require("colors");

const getStaffs = asyncHandler(async (req, res) => {
  let staffs = await Staff.find({});
  res.status(200).json(staffs);
});

const addStaff = asyncHandler(async (req, res) => {
  let findStaff = await Staff.find({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  });

  if (findStaff.length !== 0) {
    res.status(409);
    throw new Error("Duplicate record");
  } else {
    const staff = await Staff.create({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
    });
    res.status(201).json(staff);
  }
});

const showStaff = asyncHandler(async (req, res) => {
  let staff = await Staff.findById(req.params.id);
  res.status(200).json(staff);
});

const updateStaff = asyncHandler(async (req, res) => {
  let staff = await Staff.findById(req.params.id);
  if (!staff) {
    res.status(400);
    throw new Error("Staff not found");
  }

  const updatedStaff = await Staff.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  res.status(200).json(updatedStaff);
});

const disableEnableStaff = asyncHandler(async (req, res) => {
  let staff = await Staff.findById(req.params.id);
  if (!staff) {
    res.status(400);
    throw new Error("Staff not found");
  }

  let status = {
    isActive: req.body.isActive,
  };

  const updatedStaff = await Staff.findByIdAndUpdate(req.params.id, status, {
    new: true,
  });
  res.status(200).json(updatedStaff);
});

const deleteStaff = asyncHandler(async (req, res) => {
  let staff = await Staff.findById(req.params.id);
  if (!staff) {
    res.status(400);
    throw new Error("Staff not found");
  }

  const deletedStaff = await Staff.findByIdAndDelete(req.params.id);
  res.status(200).json(deletedStaff);
});

module.exports = {
  getStaffs,
  addStaff,
  showStaff,
  updateStaff,
  disableEnableStaff,
  deleteStaff,
};
