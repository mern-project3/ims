const asyncHandler = require("express-async-handler");
const auth = require("../auth");
const bcrypt = require("bcrypt");
const User = require("../models/User");
const colors = require("colors");

const getUsers = asyncHandler(async (req, res) => {
  let users = await User.find({});
  res.status(200).json(users);
});

const registerUser = asyncHandler(async (req, res) => {
  const user = await User.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
  });
  if (!req.body.firstName) {
    res.status(400);
    throw new Error("Please enter your firstname");
  }
  if (!req.body.lastName) {
    res.status(400);
    throw new Error("Please enter your lastname");
  }
  if (!req.body.email) {
    res.status(400);
    throw new Error("Please enter your email");
  }
  if (!req.body.password) {
    res.status(400);
    throw new Error("Please enter your password");
  }
  res.status(201).json(user);
});

const loginUser = asyncHandler(async (req, res) => {
  const user = await User.findOne({
    email: req.body.email,
  });
  if (user === null) {
    res.status(401).json({ access: "Authentication Failed" });
    console.log(`[Unable to login ${req.body.email} - ${new Date()}]`.red);
  } else {
    const isPasswordCorrect = bcrypt.compareSync(
      req.body.password,
      user.password
    );
    if (isPasswordCorrect) {
      res.status(200).json({ access: auth.createAccessToken(user), user });
      console.log(`[Login successful ${req.body.email} - ${new Date()}]`.green);
    } else {
      res.status(401).json({ access: "Authentication Failed" });
      console.log(`[Unable to login ${req.body.email} - ${new Date()}]`.red);
    }
  }
});

const showUser = asyncHandler(async (req, res) => {
  let user = await User.findById(req.params.id);
  res.status(200).json(user);
});

const updateUser = asyncHandler(async (req, res) => {
  let user = await User.findById(req.params.id);
  if (!user) {
    res.status(400);
    throw new Error("User not found");
  }

  const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });
  res.status(200).json(updatedUser);
});

const disableEnableUser = asyncHandler(async (req, res) => {
  let user = await User.findById(req.params.id);
  if (!user) {
    res.status(400);
    throw new Error("User not found");
  }

  let status = {
    isActive: req.body.isActive,
  };

  const updatedUser = await User.findByIdAndUpdate(req.params.id, status, {
    new: true,
  });
  res.status(200).json(updatedUser);
});

module.exports = {
  getUsers,
  registerUser,
  loginUser,
  showUser,
  updateUser,
  disableEnableUser,
};
