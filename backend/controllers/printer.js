const asyncHandler = require("express-async-handler");
const Printer = require("../models/Printer");
const colors = require("colors");

const getPrinters = asyncHandler(async (req, res) => {
  let printer = await Printer.find({});
  res.status(200).json(printer);
});

const addPrinter = asyncHandler(async (req, res) => {
  let findPrinter = await Printer.find({
    assetNumber: req.body.assetNumber,
  });

  if (findPrinter.length !== 0) {
    res.status(409);
    throw new Error("Duplicate record");
  } else {
    const printer = await Printer.create({
      assetNumber: req.body.assetNumber,
    });
    res.status(201).json(printer);
  }
});

const showPrinter = asyncHandler(async (req, res) => {
  let printer = await Printer.findById(req.params.id);
  res.status(200).json(printer);
});

const updatePrinter = asyncHandler(async (req, res) => {
  let printer = await Printer.findById(req.params.id);
  if (!printer) {
    res.status(400);
    throw new Error("Printer not found");
  }

  const updatedPrinter = await Printer.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true,
    }
  );
  res.status(200).json(updatedPrinter);
});

const deletePrinter = asyncHandler(async (req, res) => {
  let printer = await Printer.findById(req.params.id);
  if (!printer) {
    res.status(400);
    throw new Error("Printer not found");
  }

  const deletedPrinter = await Printer.findByIdAndDelete(req.params.id);
  res.status(200).json(deletedPrinter);
});

module.exports = {
  getPrinters,
  addPrinter,
  showPrinter,
  updatePrinter,
  deletePrinter,
};
