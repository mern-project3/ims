import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import Login from "./front/Login";
import Logout from "./front/Logout";
import Home from "./front/Home";
import Test from "./front/Test";

function App() {
  return (
    <div className="App">
      <Router>
        <div className="containter-fluid">
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/" element={<Home />} />
            <Route path="/test" element={<Test />} />

            <Route path="*" element={<h1>Not Found</h1>} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
