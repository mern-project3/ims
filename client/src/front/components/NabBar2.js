import React from "react";
import { useState } from "react";
import { Link } from "react-router-dom";
export default function NavBar() {
  return (
    <>
      <ul
        className="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion toogled"
        id="accordionSidebar"
      >
        {/* <!-- Sidebar - Brand --> */}
        <Link
          to={"/"}
          className="sidebar-brand d-flex align-items-center justify-content-center"
        >
          <div className="sidebar-brand-icon rotate-n-15">
            <i className="fas fa-laugh-wink"></i>
          </div>
          <div className="sidebar-brand-text mx-3">Inventory</div>
        </Link>

        {/* <!-- Heading --> */}
        <div className="sidebar-heading">Main</div>

        {/* <!-- Nav Item - Pages Collapse Menu --> */}
        <li className="nav-item">
          <Link to={"/"} className="nav-link">
            <i className="fas fa-fw fa-chart-area"></i>
            <span>Home</span>
          </Link>
        </li>

        {/* <!-- Divider --> */}
        {/* <hr className="sidebar-divider" />

        <li className="nav-item">
          <Link to={"/test"} className="nav-link">
            <i className="fas fa-fw fa-chart-area"></i>
            <span>Test</span>
          </Link>
        </li> */}

        {/* <!-- Divider --> */}
        <hr className="sidebar-divider" />

        <li className="nav-item">
          <a
            className="nav-link collapsed"
            href="#"
            data-toggle="collapse"
            data-target="#collapseTwo"
            aria-expanded="true"
            aria-controls="collapseTwo"
          >
            <i className="fas fa-fw fa-cog"></i>
            <span>Users</span>
          </a>
          <div
            id="collapseTwo"
            className="collapse"
            aria-labelledby="headingTwo"
            data-parent="#accordionSidebar"
          >
            <div className="bg-white py-2 collapse-inner rounded">
              <h6 className="collapse-header">Manage Users</h6>
              <a className="collapse-item" href="buttons.html">
                All Users
              </a>
              <a className="collapse-item" href="cards.html">
                Add User
              </a>
            </div>
          </div>
        </li>

        {/* <!-- Divider --> */}
        <hr className="sidebar-divider" />
      </ul>
    </>
  );
}
