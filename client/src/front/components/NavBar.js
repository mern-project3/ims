import React from "react";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import useUserStore from "../../store";
export default function NavBar() {
  const user = useUserStore((state) => state.user);
  return (
    <nav>
      {user.id !== null ? (
        <>
          <Link to={"/"} className="p-2">
            Home
          </Link>
          <Link to={"/test"} className="p-2">
            Test
          </Link>
          <Link className="p-2">
            {user.firstName} {user.lastName}
          </Link>
          <Link to={"/logout"} className="p-2">
            Logout
          </Link>
        </>
      ) : (
        <>
          <Link to={"/"} className="p-2">
            Home
          </Link>
          <Link to={"/test"} className="p-2">
            Test
          </Link>
          <Link to={"/login"} className="p-2">
            Login
          </Link>
        </>
      )}
    </nav>
  );
}
