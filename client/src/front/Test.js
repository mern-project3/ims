import React from "react";
import { Navigate } from "react-router-dom";
import NavBar from "./components/NabBar2";
import TopBar from "./components/TopBar";
import Footer from "./components/Footer";
import useUserStore from "../store";
export default function Test() {
  const user = useUserStore((state) => state.user);
  return (
    <>
      {user.id === null ? (
        <Navigate to={"/login"} />
      ) : (
        <div>
          <body id="page-top">
            {/* <!-- Page Wrapper --> */}
            <div id="wrapper">
              {/* <!-- Sidebar --> */}
              <NavBar />
              {/* <!-- End of Sidebar --> */}

              {/* <!-- Content Wrapper --> */}
              <div id="content-wrapper" className="d-flex flex-column">
                {/* <!-- Main Content --> */}
                <div id="content">
                  {/* <!-- Topbar --> */}
                  <TopBar />
                  {/* <!-- End of Topbar --> */}
                  {/* <!-- Begin Page Content --> */}
                  <div className="container-fluid">
                    {/* <!-- Page Heading --> */}
                    <div className="d-sm-flex align-items-center justify-content-between mb-4">
                      <h1 className="h5 mb-0 text-gray-800">Test</h1>
                    </div>

                    {/* <!-- Content Row --> */}
                    <div className="row"></div>
                  </div>
                  {/* <!-- /.container-fluid --> */}
                </div>
                {/* <!-- End of Main Content --> */}

                {/* <!-- Footer --> */}
                <Footer />
                {/* <!-- End of Footer --> */}
              </div>
              {/* <!-- End of Content Wrapper --> */}
            </div>
            {/* <!-- End of Page Wrapper --> */}

            {/* <!-- Scroll to Top Button--> */}
            <a className="scroll-to-top rounded" href="#page-top">
              <i className="fas fa-angle-up"></i>
            </a>
          </body>
        </div>
      )}
    </>
  );
}
