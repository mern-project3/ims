import React from "react";
import { Navigate } from "react-router-dom";
import useUserStore from "../store";
import { useEffect } from "react";
export default function Logout() {
  // Clear localStorage
  localStorage.clear();
  const removeUser = useUserStore((state) => state.removeUser);
  //   Fire removeUser function on store
  useEffect(() => {
    removeUser();
  });

  return <Navigate to="/" />;
}
