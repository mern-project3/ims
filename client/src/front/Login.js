import React from "react";
import { useState, useEffect } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import useUserStore from "../store";

export default function Login() {
  // States
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginBtnIsActive, setLoginBtnIsActive] = useState(false);
  const user = useUserStore((state) => state.user);
  const setUser = useUserStore((state) => state.setUser);
  const navigate = useNavigate();

  // Handles Login
  const Login = async (e) => {
    e.preventDefault();
    try {
      await fetch("http://localhost:4000/users/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.access !== "Authentication Failed") {
            saveLoginSession(data.access, data.user._id);
            // Show Notification
            toast.success("Successfully Logged In", {
              position: "top-right",
              autoClose: 3500,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "colored",
            });
            // Redirect to Home
            navigate("/");
          } else {
            // Show Notification
            toast.error("Your credentials did not match on our records!", {
              position: "top-right",
              autoClose: 3500,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "colored",
            });
          }
        });
      // Clear input fields after submission
      setEmail("");
      setPassword("");
    } catch (error) {
      console.log(error);
    }
  };

  // Handles in Get User Details

  const saveLoginSession = async (token, userId) => {
    try {
      await fetch(`http://localhost:4000/users/${userId}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          localStorage.setItem("id", data._id);
          localStorage.setItem("isAdmin", data.isAdmin);
          localStorage.setItem("firstName", data.firstName);
          localStorage.setItem("lastName", data.lastName);
          localStorage.setItem("token", token);
          localStorage.setItem("email", data.email);
          setUser(data);
        });
    } catch (error) {
      console.log(error);
    }
  };

  // UseEffect
  useEffect(() => {
    if (email.length > 0 && password.length > 0) {
      setLoginBtnIsActive(true);
    } else {
      setLoginBtnIsActive(false);
    }
  }, [email, password]);

  return (
    // {user.id !== null? <Navigate to={"/"}/> :}

    <div className="container">
      {user.id !== null ? (
        <Navigate to={"/"} />
      ) : (
        <>
          <div className="row">
            <div className="col"></div>
            <div className="col-md-6 col-sm-8 col-xs-10 col-lg-4 bg-secondary p-2 mt-5">
              <h1 className="text-white pt-3">Cocogen - IMS</h1>
              <p className="text-white pb-2 mb-5">
                Track company assets in realtime
              </p>
              <form onSubmit={(e) => Login(e)}>
                <input
                  type="text"
                  className="form-control"
                  placeholder="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <br />
                <input
                  type="password"
                  className="form-control"
                  placeholder="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <br />
                {loginBtnIsActive ? (
                  <button className="btn bg-gradient-primary w-100 mb-4 rounded-0 text-white">
                    Sign In
                  </button>
                ) : (
                  <button
                    disabled
                    className="btn bg-gradient-primary w-100 mb-4 rounded-0 text-white"
                  >
                    Sign In
                  </button>
                )}
              </form>
              <code className="text-white">
                Copyright &copy; 2023 Alrights Reserved
              </code>
            </div>

            <div className="col"></div>
          </div>
          <ToastContainer
            position="top-right"
            autoClose={900}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
          />
        </>
      )}
    </div>
  );
}
