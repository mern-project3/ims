import { create } from "zustand";

const useUserStore = create((set) => ({
  user: {
    id: localStorage.getItem("id"),
    isAdmin: localStorage.getItem("isAdmin"),
    firstName: localStorage.getItem("firstName"),
    lastName: localStorage.getItem("lastName"),
    token: localStorage.getItem("token"),
    email: localStorage.getItem("email"),
  },
  //   setUser: (data) => set((state) => ({ user: data })),
  setUser: (data) => set({ user: data }),
  removeUser: () =>
    set({
      user: {
        id: localStorage.getItem("id"),
        isAdmin: localStorage.getItem("isAdmin"),
        firstName: localStorage.getItem("firstName"),
        lastName: localStorage.getItem("lastName"),
        token: localStorage.getItem("token"),
        email: localStorage.getItem("email"),
      },
    }),
}));

export default useUserStore;
